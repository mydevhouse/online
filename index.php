<?php include('header.php');?>
<body class="index">
<?php include('/includes/menu.php');?>

<div id="section-1">
    <div class="container-fluid">
        <div class="jumbotron hero-technology">
        <h1 class="hero-title">Sample Title</h1>
        <p class="hero-subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam numquam voluptates fugit consequuntur perferendis asperiores, cumque, nesciunt laboriosam porro similique eveniet earum error itaque maxime labore praesentium nulla sit magnam.</p>
        <p><a class="btn btn-primary btn-lg hero-button" role="button" href="#">Learn more</a></p>
        </div>
    </div>
</div>

<div class="section-2 p50">
    <div class="container">
        <div class="col-md-12">
           <div class="text-center">
               <h1>Title</h1>
               <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis illum amet quod dolores repellat nesciunt. Similique reprehenderit natus sapiente provident necessitatibus. Quo distinctio sapiente quia necessitatibus quis corporis rerum inventore.</span>
               <span>Doloribus consequatur adipisci laudantium perferendis esse nisi explicabo, autem provident error. Labore aliquid vero eligendi amet, doloribus in voluptatem. Eaque, sint. Aut hic aliquam iusto aspernatur optio nisi sed autem.</span>
               <span>Voluptates voluptate molestiae vero, accusantium ratione culpa ipsam dolor aspernatur. Natus repellat, officiis inventore error nam blanditiis voluptatibus nisi nesciunt explicabo libero, eum quae. Facere fugit quos harum, corrupti velit.</span>
               <span>Voluptas qui sequi id omnis deleniti est esse asperiores, eos ab sed assumenda vel architecto soluta aut, sit nesciunt. Porro beatae, velit unde quod ab obcaecati esse incidunt laudantium ipsa.</span>
               <span>Impedit aliquam dolorum, vel nihil illo rem iusto libero quod consequuntur perferendis, voluptatem accusantium quas ipsam quam commodi eos! Nemo, cum, laboriosam. Molestias ea impedit magnam, laborum dolorum dolorem fugit.</span></p>
               <a href="#" class="btn btn-primary btn-lg">Read More >></a>
           </div>
            
        </div>
    </div>
</div>

<div class="section-3 p50">
    <div class="container">
        <div class="col-md-12 text-center">
            <h1>title</h1>
        </div>
    </div>
    <div class="container-fluid">
        <div class="col-md-4"><img src="http://placehold.it/500x500" class="img-responsive" alt=""></div>
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
    </div>
</div>


<?php include('footer.php');?>